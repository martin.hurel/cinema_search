import React, { Fragment } from 'react';
import PropTypes from 'prop-types'; 
import styled from '@emotion/styled';
import { Container, Div, H2, Img, Span} from './style';

const Movie = ({data}) => (
<Container>
    <Div>
        <H2>{data.Title}</H2>
        <p><Span>Date de sorties: </Span>{data.Released !== 'N/A' ? data.Released : 'Date de sorties non connus !'}</p>
        <p><Span>Durée: </Span>{data.Runtime !== 'N/A' ? data.Runtime : 'Aucune informations sur la durée du film !'}</p>
        <p><Span>Acteurs: </Span>{data.Actors !== 'N/A' ? data.Actors : 'Aucune informations sur les acteurs du film !'}</p>
        <p><Span>Rated: </Span>{data.Rated !== 'N/A' ? data.Rated : 'Pas d\'informations pour le rated'}</p>
        <p><Span>Récompenses: </Span>{data.Awards !== 'N/A' ? data.Awards : 'Aucune récompenses'}</p>
        <p><Span>Chiffre au boxoffice: </Span>{data.BoxOffice !== 'N/A' ? data.BoxOffice : 'Aucune information sur le chiffre au boxoffice'}</p>
        <p><Span>Date de sorties en DVD : </Span>{data.DVD !== 'N/A' ? data.DVD : 'Pas d\'informations sur la date de sortis en DVD'}</p>
        <p><Span>Réalisateur: </Span>{data.Director !== 'N/A' ? data.Director : 'Pas d\'informations sur le réalisateur'}</p>
        <p><Span>Genre: </Span>{data.Genre !== 'N/A' ? data.Genre : 'Pas d\'informations sur le genre du film'}</p>
        <p><Span>Metascore: </Span>{data.Metascore !== 'N/A' ? data.Metascore : 'Pas d\'informations sur le metascore du film'}</p>
        <p><Span>Résumé: </Span>{data.Plot !== 'N/A' ? data.Plot : 'Pas de résumé disponible pour ce film'}</p>
        <p><Span>Maison de production: </Span>{data.Production !== 'N/A' ? data.Production : 'Pas d\'information sur la maison de production'}</p>
        <p><Span>Note IMDB: </Span>{data.imdbRating !== 'N/A' ? data.imdbRating : 'IMDB n\'a pas noté ce site !'}</p>
    </Div>
    <Img src={data.Poster.length > 3 ? data.Poster : 'http://mhurel.eemi.tech/A4%20(1).png'}/>
</Container>
);

export default Movie;