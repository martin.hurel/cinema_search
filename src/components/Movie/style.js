import styled from '@emotion/styled';
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
export const Div = styled.div`
    width:50%;
    margin:0 0 0 10px;
`

export const H2 = styled.h2`
    text-align:center;
    font-weight:bold;
    font-size:35px;
    margin-top:40px;
`

export const Span = styled.span`
    font-weight:bold;
`

export const Img = styled.img`
    width:350px;
    height: 450px;
    margin:auto;
`

export const Container = styled.div`
    display: flex;
    width: 96%;
    margin: auto;
    box-shadow: 2px 2px 10px 1px rgba(0, 0, 255, .4);
    padding: 10px;
    margin: 8px;
`
export const Poster = styled.img`
    width: 150px;
    height:250px;
`
export const Bouton = styled.button`
    width:15%;
    height:25px;
    margin:auto;
    margin-top: 15px;
    margin-bottom:15px;
    background:white;
    color:black;
    border:1px solid silver;
    border-radius:20px;
    font-weight:bold;
`

export const Input = styled.input`
    width: 90%;
    height:20px;
    border-radius:15px;
    border:0.5 solid black;
    margin:auto;
    padding:5px;
`
export const Form = styled.form`
    display:flex;
    flex-direction:column;
`

export const Sup = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
`

export const Alert = styled.p`
    color: red;
    width: 100%;
    text-align: center;
    font-size: 20px;
    font-weight: 'bold';
`

export const StyledLink= styled(Link)`
    :hover{
        box-shadow: 2px 2px 10px 1px rgba(0, 0, 255, .4);
    }
    width: 15%;
    padding: 10px 0 0 0;
    margin: 10px 10px 10px 10px;
    list-style-type: none;
    text-align: center;
    diplay: flex;
    flex-direction: column;
    color:black;
    font-size:20px;
    text-decoration:none;
`

export const H1 = styled.h1`
    text-align:center;
    text-decoration:none;
    font-size:35px;
    margin:auto;
    display:flex;
    justify-content:center;
    font-weight:bold;
`

export const Li = styled.li`
    list-style-type: none;
`