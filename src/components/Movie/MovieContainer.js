import React, { Component} from 'react';
import { withRouter} from 'react-router-dom';
import Movie from './Movie';

class MovieContainer extends Component{
    state = {
        movie: null,
    }

    componentDidMount(){
        const { id } = this.props.match.params;
        console.log(id);
        console.log(this.props.match.params);
        const url = `http://www.omdbapi.com/?apikey=7dafeb89&i=${id}`;
        fetch(url)
        .then(response => response.json())
        .then(data => {
          this.setState({movie: data})
          console.log(data);
        })
        .catch(error => console.error(error))
    }
    render(){
        if(!this.state.movie) return null;
        return <Movie 
        data={this.state.movie}
        />
    }
}

export default withRouter(MovieContainer);