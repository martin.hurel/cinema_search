import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import HomeContainer from '../Home/HomeContainer';
import MovieContainer from '../Movie/MovieContainer';
import Redirect from 'react-router-dom/Redirect';
import styled from '@emotion/styled';
import { H1, Li} from '../Movie/style';


const App = () => (
  <Router>
    <div className="container">
      <nav>
        <ul>
          <Li>
            <Link to="/"><H1>Cinema Research</H1></Link>
          </Li>
        </ul>
      </nav>
      <div className="App">
        <Route exact path="/" component={HomeContainer} />
        <Route exact path="/movie/:id" component={MovieContainer}/>
      </div>
    </div>
  </Router>
);

export default App;
