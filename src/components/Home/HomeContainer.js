import React, { Component, createRef } from 'react';
import Home from './Home';
import {apiUrl} from '../../constants/config';
import { createRequireFromPath } from 'module';

class HomeContainer extends Component {
  state = {movies: [], error: null, data: null}
  searchInput = createRef();

  submitForm = (event) => {
    event.preventDefault();
    const query = this.searchInput.current.value;
    const url = apiUrl(query);
    fetch(url)
    .then(response => response.json())
    .then(data => {
      this.setState({ data })
    })
    .catch(error => console.error(error))
  }
  render() {
    console.log(this.state.data);
    if(this.state.data){
      console.log('First condition')
      if(this.state.data.Response == "False"){
        console.log('je suis false')
          return <Home searchInput={this.searchInput} 
          data={this.state.data}
          submitValue={this.submitForm}
          error="Aucun film trouvé !"/>;
        }
        else{
          console.log('on est la')
          return <Home searchInput={this.searchInput} 
          data={this.state.data}
          submitValue={this.submitForm}/>;
        }
    }
    else{
      console.log('DOMMACH')
      return <Home searchInput={this.searchInput} 
          data={this.state.data}
          submitValue={this.submitForm}/>;
    }
  }
}

export default HomeContainer;
