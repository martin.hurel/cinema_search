import React, { Fragment } from 'react';
import PropTypes from 'prop-types'; 
import { Link } from 'react-router-dom';
import styled from '@emotion/styled';
import { Poster, Bouton, Input, Form, Sup, Alert} from '../Movie/style';
import {StyledLink} from '../Movie/style';

const Home = ({searchInput, submitValue, data, error}) => (
  <Fragment>
    <Form onSubmit={submitValue}>
    <Input ref={searchInput}  required placeholder="Rentrer le nom d'un film"/>
    <Bouton type="submit">Rechercher</Bouton>
    </Form>
    {error 
      ? <Sup><Alert>Aucun film trouvé ! Réessayer !</Alert></Sup>
      : <Sup>
          {data
                ? data.Search.map((item) => <StyledLink to={`/movie/${item.imdbID}`}><Poster src={item.Poster.length > 3 ? item.Poster : 'http://mhurel.eemi.tech/A4%20(1).png'}/><br></br>{item.Title}</StyledLink>) 
                : ""
          }
        </Sup>
    }
    
  </Fragment>
);

Home.propTypes = {
  searchInput: PropTypes.object.isRequired,
  submitValue: PropTypes.func.isRequired,
};



export default Home;


